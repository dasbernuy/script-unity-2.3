using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public GameObject camerasParent;        //Parent object of all cameras that should rotate with mouse 
    public float hRotationSpeed = 100f;     //Player rotates along y axis
    public float vRotationSpeed = 80f;      //Cam rotates along x axis
    public float maxVerticalAngle;          //Maximun rotation along x axis 
    public float minVerticalAngle;          //Minimum rotation along x axis
    public float smoothTime = 0.05f;

    float vCamRotationAngles;               //variable to apply Vertical Rotation
    float hPlayerRotation;                  //variable to apply Horizontal Rotation
    float CurrentHVelocity;                 //smooth horizontal velocity 
    float CurrentVVelocity;                 //smooth vertical velocity
    float targetCamEulers;                  //variable to accumulate the eurler angles along x axis 
    Vector3 targetCamRotation;              /* aux variable to store the TargetRotation of the camerasParent 
                                                avoiding to instatiate a new Vector 3 every Frame */

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void handleRotation(float hInput, float vInput)
    {
        float targetPlayerRotation = hInput * hRotationSpeed * Time.deltaTime;
        targetCamEulers += vInput * vRotationSpeed * Time.deltaTime;

        // Next up, the player rotation.
        hPlayerRotation = Mathf.SmoothDamp(hPlayerRotation, targetPlayerRotation, ref CurrentHVelocity, smoothTime);
        transform.Rotate(0f, hPlayerRotation, 0f);

        // Now it is the time for Cam Rotation.
        targetCamEulers = Mathf.Clamp(targetCamEulers, minVerticalAngle, maxVerticalAngle);
        vCamRotationAngles = Mathf.SmoothDamp(vCamRotationAngles, targetCamEulers, ref CurrentVVelocity, smoothTime);
        targetCamRotation.Set(-vCamRotationAngles, 0f, 0f);
        camerasParent.transform.localEulerAngles = targetCamRotation;
    }
}
