using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTankController : MonoBehaviour
{

    private CharacterMovement characterMovement;
    private MouseLook mouselook;

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").gameObject.SetActive(false);

        characterMovement = GetComponent<CharacterMovement>();
        mouselook = GetComponent<MouseLook>();
    }

    // Update is called once per frame
    private void Update()
    {
        Movement();
        Rotation();
    }

    private void Movement()
    {

        //Movement
        float hMovementInput = Input.GetAxisRaw("Horizontal");
        float vMovementInput = Input.GetAxisRaw("Vertical");

        bool jumpInput = Input.GetButtonDown("Jump");
        bool dashInput = Input.GetButton("Dash");

        characterMovement.moveCharacter(hMovementInput, vMovementInput, jumpInput, dashInput);
    }

    private void Rotation()
    {
        //Rotation
        float hRotationInput = Input.GetAxis("Mouse X");
        float vRotationInput = Input.GetAxis("Mouse Y");

        mouselook.handleRotation(hRotationInput, vRotationInput);
    }
}

